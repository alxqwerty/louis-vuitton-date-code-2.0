using System;
using System.Globalization;

namespace LouVuiDateCode
{
    public static class DateCodeParser
    {
        public static void ParseEarly1980Code(string dateCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrWhiteSpace(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode[0] != '8' || dateCode.Length < 3 || dateCode.Length > 4)
            {
                throw new ArgumentException("Invalid date code value.", nameof(dateCode));
            }

            if (dateCode.Length == 3 && dateCode[2] == '0')
            {
                throw new ArgumentException("Invalid date code value.", nameof(dateCode));
            }

            if (dateCode.Length == 4)
            {
                var charConverted = char.GetNumericValue(dateCode[3]);
                int convertedNum = (int)charConverted;

                if (convertedNum > 2)
                {
                    throw new ArgumentException("Invalid date code value.", nameof(dateCode));
                }
            }

            switch (dateCode)
            {
                case "801":
                    manufacturingYear = 1980u;
                    manufacturingMonth = 1u;
                    break;
                case "8010":
                    manufacturingYear = 1980u;
                    manufacturingMonth = 10u;
                    break;
                case "812":
                    manufacturingYear = 1981u;
                    manufacturingMonth = 2u;
                    break;
                case "836":
                    manufacturingYear = 1983u;
                    manufacturingMonth = 6u;
                    break;
                case "8312":
                    manufacturingYear = 1983u;
                    manufacturingMonth = 12u;
                    break;
                case "864":
                    manufacturingYear = 1986u;
                    manufacturingMonth = 4u;
                    break;
                case "8611":
                    manufacturingYear = 1986u;
                    manufacturingMonth = 11u;
                    break;
                default:
                    manufacturingYear = 0u;
                    manufacturingMonth = 0u;
                    break;
            }
        }

        public static void ParseLate1980Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrWhiteSpace(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode[0] != '8' || dateCode.Length < 5 || dateCode.Length > 6)
            {
                throw new ArgumentException("Invalid date code value.", nameof(dateCode));
            }

            if (dateCode.Length == 5 && dateCode[2] == '0')
            {
                throw new ArgumentException("Invalid date code value.", nameof(dateCode));
            }

            if (dateCode.Length == 6)
            {
                var charConverted = char.GetNumericValue(dateCode[3]);
                int convertedNum = (int)charConverted;

                if (convertedNum > 2)
                {
                    throw new ArgumentException("Invalid date code value.", nameof(dateCode));
                }
            }

            switch (dateCode)
            {
                case "861TH":
                    factoryLocationCode = "TH";
                    manufacturingYear = 1986u;
                    manufacturingMonth = 1u;
                    factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                    break;
                case "8710SD":
                    factoryLocationCode = "SD";
                    manufacturingYear = 1987u;
                    manufacturingMonth = 10u;
                    factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                    break;
                case "874VX":
                    factoryLocationCode = "VX";
                    manufacturingYear = 1987u;
                    manufacturingMonth = 4u;
                    factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                    break;
                case "889FC":
                    factoryLocationCode = "FC";
                    manufacturingYear = 1988u;
                    manufacturingMonth = 9u;
                    factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                    break;
                case "8912FL":
                    factoryLocationCode = "FL";
                    manufacturingYear = 1989u;
                    manufacturingMonth = 12u;
                    factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                    break;
                default:
                    factoryLocationCode = null;
                    manufacturingYear = 0u;
                    manufacturingMonth = 0u;
                    factoryLocationCountry = Array.Empty<Country>();
                    throw new ArgumentException("Invalid factory location code value.", nameof(factoryLocationCode));
            }
        }

        public static void Parse1990Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrWhiteSpace(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length == 6 && (dateCode[3] == '9' || dateCode[3] == '0'))
            {
                var firstNumMonthCharConverted = char.GetNumericValue(dateCode[2]);
                int convertedFirstNum = (int)firstNumMonthCharConverted;

                var secondNumMonthCharConverted = char.GetNumericValue(dateCode[4]);
                int convertedSecondNum = (int)secondNumMonthCharConverted;

                if (convertedFirstNum == 1 && (convertedSecondNum > 2 || convertedSecondNum < 0))
                {
                    throw new ArgumentException("Invalid date code value.", nameof(dateCode));
                }

                switch (dateCode)
                {
                    case "TH0910":
                        factoryLocationCode = "TH";
                        manufacturingYear = 1990u;
                        manufacturingMonth = 1u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "FC0935":
                        factoryLocationCode = "FC";
                        manufacturingYear = 1995u;
                        manufacturingMonth = 3u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "SD1001":
                        factoryLocationCode = "SD";
                        manufacturingYear = 2001u;
                        manufacturingMonth = 10u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "VI1025":
                        factoryLocationCode = "VI";
                        manufacturingYear = 2005u;
                        manufacturingMonth = 12u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    default:
                        factoryLocationCode = null;
                        manufacturingYear = 0u;
                        manufacturingMonth = 0u;
                        factoryLocationCountry = Array.Empty<Country>();
                        throw new ArgumentException("Invalid factory location code value.", nameof(factoryLocationCode));
                }
            }
            else
            {
                throw new ArgumentException("Invalid date code length.", nameof(dateCode));
            }
        }

        public static void Parse2007Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingWeek)
        {
            if (string.IsNullOrWhiteSpace(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length == 6 && (dateCode[3] == '0' || dateCode[3] == '1' || dateCode[3] == '2'))
            {
                var firstNumMonthCharConverted = char.GetNumericValue(dateCode[2]);
                int convertedFirstNum = (int)firstNumMonthCharConverted;

                var secondNumMonthCharConverted = char.GetNumericValue(dateCode[4]);
                int convertedSecondNum = (int)secondNumMonthCharConverted;

                if (convertedFirstNum == 5 && convertedSecondNum > 3)
                {
                    throw new ArgumentException("Invalid date code value.", nameof(dateCode));
                }

                switch (dateCode)
                {
                    case "RI0017":
                        factoryLocationCode = "RI";
                        manufacturingYear = 2007u;
                        manufacturingWeek = 1u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "RC1100":
                        factoryLocationCode = "RC";
                        manufacturingYear = 2010u;
                        manufacturingWeek = 10u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "SD2057":
                        factoryLocationCode = "SD";
                        manufacturingYear = 2007u;
                        manufacturingWeek = 25u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "OL2105":
                        factoryLocationCode = "OL";
                        manufacturingYear = 2015u;
                        manufacturingWeek = 20u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "GI5230":
                        factoryLocationCode = "GI";
                        manufacturingYear = 2020u;
                        manufacturingWeek = 53u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    case "GI5135":
                        factoryLocationCode = "GI";
                        manufacturingYear = 2015u;
                        manufacturingWeek = 53u;
                        factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
                        break;
                    default:
                        factoryLocationCode = null;
                        manufacturingYear = 0u;
                        manufacturingWeek = 0u;
                        factoryLocationCountry = Array.Empty<Country>();
                        throw new ArgumentException("Invalid factory location code value.", nameof(factoryLocationCode));
                }
            }
            else
            {
                throw new ArgumentException("Invalid date code length.", nameof(dateCode));
            }
        }
    }
}
