using System;
using System.Globalization;
using System.Text;

namespace LouVuiDateCode
{
    public static class DateCodeGenerator
    {
        public static string GenerateEarly1980Code(uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingYear < 1980 || manufacturingYear > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth <= 0 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            StringBuilder container = new StringBuilder();

            string convYear = manufacturingYear.ToString("G", CultureInfo.CurrentCulture);
            string convMonth = manufacturingMonth.ToString("G", CultureInfo.CurrentCulture);

            container.Append(convYear[2]);
            container.Append(convYear[3]);
            container.Append(convMonth);

            string luviCode = container.ToString();

            return luviCode;
        }

        public static string GenerateEarly1980Code(DateTime manufacturingDate)
        {
            if (manufacturingDate < DateTime.Parse("1/1/1980", CultureInfo.CurrentCulture) || manufacturingDate > DateTime.Parse("12/31/1989", CultureInfo.CurrentCulture))
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            StringBuilder container = new StringBuilder();

            string convDate = manufacturingDate.ToString("M/d/yyyy", CultureInfo.CurrentCulture);

            container.Append(convDate[^2]);
            container.Append(convDate[^1]);

            if (convDate[1] == '/')
            {
                container.Append(convDate[0]);
            }
            else
            {
                container.Append(convDate[0]);
                container.Append(convDate[1]);
            }

            string luviCode = container.ToString();

            return luviCode;
        }

        public static string GenerateLate1980Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingYear < 1980 || manufacturingYear > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth <= 0 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2)
            {
                throw new ArgumentException("Invalid factory location code length value.", nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Contains('0', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('1', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('2', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('3', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('4', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('5', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('6', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('7', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('8', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('9', StringComparison.CurrentCulture))
            {
                throw new ArgumentException("Invalid symbol (int) in factory location code.", nameof(factoryLocationCode));
            }

            StringBuilder container = new StringBuilder();

            string convYear = manufacturingYear.ToString("G", CultureInfo.CurrentCulture);
            string convMonth = manufacturingMonth.ToString("G", CultureInfo.CurrentCulture);

            container.Append(convYear[2]);
            container.Append(convYear[3]);
            container.Append(convMonth);
            container.Append(factoryLocationCode);

            string luviCode = container.ToString();

            return luviCode.ToUpper(CultureInfo.CurrentCulture);
        }

        public static string GenerateLate1980Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate < DateTime.Parse("1/1/1980", CultureInfo.CurrentCulture) || manufacturingDate > DateTime.Parse("12/31/1989", CultureInfo.CurrentCulture))
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (factoryLocationCode.Length != 2)
            {
                throw new ArgumentException("Invalid factory location code length value.", nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Contains('0', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('1', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('2', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('3', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('4', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('5', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('6', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('7', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('8', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('9', StringComparison.CurrentCulture))
            {
                throw new ArgumentException("Invalid symbol (int) in factory location code.", nameof(factoryLocationCode));
            }

            StringBuilder container = new StringBuilder();

            string convDate = manufacturingDate.ToString("M/d/yyyy", CultureInfo.CurrentCulture);

            container.Append(convDate[^2]);
            container.Append(convDate[^1]);

            if (convDate[1] == '/')
            {
                container.Append(convDate[0]);
            }
            else
            {
                container.Append(convDate[0]);
                container.Append(convDate[1]);
            }

            container.Append(factoryLocationCode);

            string luviCode = container.ToString();

            return luviCode.ToUpper(CultureInfo.CurrentCulture);
        }

        public static string Generate1990Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingYear < 1990 || manufacturingYear > 2006)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth <= 0 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2)
            {
                throw new ArgumentException("Invalid factory location code length value.", nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Contains('0', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('1', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('2', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('3', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('4', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('5', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('6', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('7', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('8', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('9', StringComparison.CurrentCulture))
            {
                throw new ArgumentException("Invalid symbol (int) in factory location code.", nameof(factoryLocationCode));
            }

            StringBuilder container = new StringBuilder();

            string convYear = manufacturingYear.ToString("G", CultureInfo.CurrentCulture);
            string convMonth = manufacturingMonth.ToString("G", CultureInfo.CurrentCulture);

            container.Append(factoryLocationCode);

            if (manufacturingMonth < 10)
            {
                container.Append('0');
                container.Append(convYear[2]);
                container.Append(convMonth);
                container.Append(convYear[3]);
            }

            if (manufacturingMonth >= 10 && manufacturingMonth <= 12)
            {
                container.Append(convMonth[0]);
                container.Append(convYear[2]);
                container.Append(convMonth[1]);
                container.Append(convYear[3]);
            }

            string luviCode = container.ToString();

            return luviCode.ToUpper(CultureInfo.CurrentCulture);
        }

        public static string Generate1990Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate < DateTime.Parse("1/1/1990", CultureInfo.CurrentCulture) || manufacturingDate > DateTime.Parse("12/31/2006", CultureInfo.CurrentCulture))
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (factoryLocationCode.Length != 2)
            {
                throw new ArgumentException("Invalid factory location code length value.", nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Contains('0', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('1', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('2', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('3', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('4', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('5', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('6', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('7', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('8', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('9', StringComparison.CurrentCulture))
            {
                throw new ArgumentException("Invalid symbol (int) in factory location code.", nameof(factoryLocationCode));
            }

            StringBuilder container = new StringBuilder();

            string convDate = manufacturingDate.ToString("MM/d/yyyy", CultureInfo.CurrentCulture);

            container.Append(factoryLocationCode);

            container.Append(convDate[0]);
            container.Append(convDate[^2]);
            container.Append(convDate[1]);
            container.Append(convDate[^1]);

            string luviCode = container.ToString();

            return luviCode.ToUpper(CultureInfo.CurrentCulture);
        }

        public static string Generate2007Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingWeek)
        {
            if (manufacturingYear < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingWeek == 53)
            {
                switch (manufacturingYear)
                {
                    case 2009:
                        break;
                    case 2015:
                        break;
                    case 2020:
                        break;
                    case 2026:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(manufacturingWeek));
                }
            }

            if (manufacturingWeek <= 0 || manufacturingWeek > 53)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingWeek));
            }

            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Length != 2)
            {
                throw new ArgumentException("Invalid factory location code length value.", nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Contains('0', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('1', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('2', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('3', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('4', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('5', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('6', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('7', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('8', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('9', StringComparison.CurrentCulture))
            {
                throw new ArgumentException("Invalid symbol (int) in factory location code.", nameof(factoryLocationCode));
            }

            StringBuilder container = new StringBuilder();

            string convYear = manufacturingYear.ToString("G", CultureInfo.CurrentCulture);
            string convWeek = manufacturingWeek.ToString("G", CultureInfo.CurrentCulture);

            container.Append(factoryLocationCode);

            if (manufacturingWeek < 10)
            {
                container.Append('0');
                container.Append(convYear[2]);
                container.Append(convWeek);
                container.Append(convYear[3]);
            }

            if (manufacturingWeek >= 10 && manufacturingWeek <= 53)
            {
                container.Append(convWeek[0]);
                container.Append(convYear[2]);
                container.Append(convWeek[1]);
                container.Append(convYear[3]);
            }

            string luviCode = container.ToString();

            return luviCode.ToUpper(CultureInfo.CurrentCulture);
        }

        public static string Generate2007Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate < DateTime.Parse("1/1/2007", CultureInfo.CurrentCulture))
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (factoryLocationCode.Length != 2)
            {
                throw new ArgumentException("Invalid factory location code length value.", nameof(factoryLocationCode));
            }

            if (factoryLocationCode.Contains('0', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('1', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('2', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('3', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('4', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('5', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('6', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('7', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('8', StringComparison.CurrentCulture)
                || factoryLocationCode.Contains('9', StringComparison.CurrentCulture))
            {
                throw new ArgumentException("Invalid symbol (int) in factory location code.", nameof(factoryLocationCode));
            }

            StringBuilder container = new StringBuilder();
            int year = ISOWeek.GetYear(manufacturingDate);
            int week = ISOWeek.GetWeekOfYear(manufacturingDate);
            string convWeek = week.ToString(CultureInfo.CurrentCulture);

            container.Append(factoryLocationCode);

            if (week < 10)
            {
                string convYear = year.ToString("G", CultureInfo.CurrentCulture);
                container.Append('0');
                container.Append(convYear[^2]);
                container.Append(convWeek[0]);
                container.Append(convYear[^1]);
            }
            else if (ISOWeek.GetWeekOfYear(manufacturingDate) == 53 && (year == 2009 || year == 2015 || year == 2020 || year == 2026))
            {
                string convYear = year.ToString("G", CultureInfo.CurrentCulture);
                container.Append(convWeek[0]);
                container.Append(convYear[^2]);
                container.Append(convWeek[1]);
                container.Append(convYear[^1]);
            }
            else
            {
                year = ISOWeek.GetYear(manufacturingDate);
                string convYear = year.ToString("G", CultureInfo.CurrentCulture);
                container.Append(convWeek[0]);
                container.Append(convYear[^2]);
                container.Append(convWeek[1]);
                container.Append(convYear[^1]);
            }

            string luviCode = container.ToString();

            return luviCode.ToUpper(CultureInfo.CurrentCulture);
        }
    }
}
