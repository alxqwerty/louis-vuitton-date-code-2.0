﻿using System;

namespace LouVuiDateCode
{
    public static class CountryParser
    {
        public static Country[] GetCountry(string factoryLocationCode)
        {
            if (string.IsNullOrWhiteSpace(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            Country[] countries = Array.Empty<Country>();
            int countCountries = 0;

            if (factoryLocationCode == "A0" || factoryLocationCode == "A1" || factoryLocationCode == "A2"
                || factoryLocationCode == "AA" || factoryLocationCode == "AAS" || factoryLocationCode == "AH"
                || factoryLocationCode == "AN" || factoryLocationCode == "AR" || factoryLocationCode == "AS"
                || factoryLocationCode == "BA" || factoryLocationCode == "BJ" || factoryLocationCode == "BU"
                || factoryLocationCode == "DR" || factoryLocationCode == "DU" || factoryLocationCode == "DT"
                || factoryLocationCode == "CO" || factoryLocationCode == "CT" || factoryLocationCode == "CX"
                || factoryLocationCode == "ET" || factoryLocationCode == "FL" || factoryLocationCode == "LA"
                || factoryLocationCode == "LW" || factoryLocationCode == "MB" || factoryLocationCode == "MI"
                || factoryLocationCode == "NO" || factoryLocationCode == "RA" || factoryLocationCode == "RI"
                || factoryLocationCode == "SA" || factoryLocationCode == "SD" || factoryLocationCode == "SF"
                || factoryLocationCode == "SL" || factoryLocationCode == "SN" || factoryLocationCode == "SP"
                || factoryLocationCode == "SR" || factoryLocationCode == "TA" || factoryLocationCode == "TJ"
                || factoryLocationCode == "TH" || factoryLocationCode == "TN" || factoryLocationCode == "TR"
                || factoryLocationCode == "TS" || factoryLocationCode == "VI" || factoryLocationCode == "VX")
            {
                countCountries++;
                Country[] tempCountries = new Country[countCountries];

                if (countCountries == 1)
                {
                    for (int i = 0; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.France;
                    }
                }
                else
                {
                    for (int i = 0; i < tempCountries.Length - 1; i++)
                    {
                        tempCountries[i] = countries[i];
                    }

                    for (int i = tempCountries.Length - 1; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.France;
                    }
                }

                countries = tempCountries;
            }

            if (factoryLocationCode == "LP" || factoryLocationCode == "OL")
            {
                countCountries++;
                Country[] tempCountries = new Country[countCountries];

                if (countCountries == 1)
                {
                    for (int i = 0; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Germany;
                    }
                }
                else
                {
                    for (int i = 0; i < tempCountries.Length - 1; i++)
                    {
                        tempCountries[i] = countries[i];
                    }

                    for (int i = tempCountries.Length - 1; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Germany;
                    }
                }

                countries = tempCountries;
            }

            if (factoryLocationCode == "BC" || factoryLocationCode == "BO" || factoryLocationCode == "CE"
                || factoryLocationCode == "FN" || factoryLocationCode == "FO" || factoryLocationCode == "MA"
                || factoryLocationCode == "NZ" || factoryLocationCode == "OB" || factoryLocationCode == "PL"
                || factoryLocationCode == "RC" || factoryLocationCode == "RE" || factoryLocationCode == "SA"
                || factoryLocationCode == "TD")
            {
                countCountries++;
                Country[] tempCountries = new Country[countCountries];

                if (countCountries == 1)
                {
                    for (int i = 0; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Italy;
                    }
                }
                else
                {
                    for (int i = 0; i < tempCountries.Length - 1; i++)
                    {
                        tempCountries[i] = countries[i];
                    }

                    for (int i = tempCountries.Length - 1; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Italy;
                    }
                }

                countries = tempCountries;
            }

            if (factoryLocationCode == "BC" || factoryLocationCode == "CA" || factoryLocationCode == "LO"
                || factoryLocationCode == "LB" || factoryLocationCode == "LM" || factoryLocationCode == "LW"
                || factoryLocationCode == "GI" || factoryLocationCode == "UB")
            {
                countCountries++;
                Country[] tempCountries = new Country[countCountries];

                if (countCountries == 1)
                {
                    for (int i = 0; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Spain;
                    }
                }
                else
                {
                    for (int i = 0; i < tempCountries.Length - 1; i++)
                    {
                        tempCountries[i] = countries[i];
                    }

                    for (int i = tempCountries.Length - 1; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Spain;
                    }
                }

                countries = tempCountries;
            }

            if (factoryLocationCode == "DI" || factoryLocationCode == "FA")
            {
                countCountries++;
                Country[] tempCountries = new Country[countCountries];

                if (countCountries == 1)
                {
                    for (int i = 0; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Switzerland;
                    }
                }
                else
                {
                    for (int i = 0; i < tempCountries.Length - 1; i++)
                    {
                        tempCountries[i] = countries[i];
                    }

                    for (int i = tempCountries.Length - 1; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.Switzerland;
                    }
                }

                countries = tempCountries;
            }

            if (factoryLocationCode == "FC" || factoryLocationCode == "FH" || factoryLocationCode == "LA"
                || factoryLocationCode == "OS" || factoryLocationCode == "SD" || factoryLocationCode == "FL"
                || factoryLocationCode == "TX")
            {
                countCountries++;
                Country[] tempCountries = new Country[countCountries];

                if (countCountries == 1)
                {
                    for (int i = 0; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.USA;
                    }
                }
                else
                {
                    for (int i = 0; i < tempCountries.Length - 1; i++)
                    {
                        tempCountries[i] = countries[i];
                    }

                    for (int i = tempCountries.Length - 1; i < tempCountries.Length; i++)
                    {
                        tempCountries[i] = Country.USA;
                    }
                }

                countries = tempCountries;
            }

            return countries;
        }
    }
}
